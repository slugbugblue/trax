# Trax

This project provides the ability to work with Trax games in Javascript.

## Description

Trax is a two-player boardless board game invented by David Smith. For more
information about Trax, including its [rules][traxrules] and
[history][traxhistory], see the official website at [traxgame.com][traxgame].

The goal of this javascript project is to allow Trax to be played
programmatically. It can be used as an engine in a web browser, for example at
[slugbugblue.com][sbb]; or in nodejs, for example by using the [trax
CLI][trax-cli] provided as a child of this project.

The Trax engine now enjoys automated testing with 100% code coverage. Any future
changes should meet the same standard.

## API Usage

The Trax engine can be used as follows:

```javascript
import { Trax } from '@slugbugblue/trax'

// default constructor gives you a Trax game
let trax = new Trax()
trax.play('@0/')

// but you can also select Loop Trax
let loops = new Trax('traxloop')

// or 8x8 Trax
let tiny = new Trax('trax8')

// and the constructor lets you start with an initial set of moves
let puzzle = new Trax('trax', '@0/ a0\\ @2/')
puzzle.play('@2\\')
```

For specifics, see the [engine.js documentation][docs].

## Support

This project is built by [Chad Transtrum][ctrans] for [slugbugblue.com][sbb].
Issues can be opened on the [gitlab project page][repo].

## Contributing

[Contributions are welcome][contributing]. Ideally in the form of pull requests,
but feel free to open an issue with a bug report or a suggestion as well.

## Acknowledgments

Many thanks to the late David Smith for coming up with the idea of Trax and for
his generosity in allowing me to add the game to [GoldToken.com][goldtoken].

Thanks to his widow Colleen Foley-Smith for extending that courtesy to allow me
to include Trax on [slugbugblue.com][sbb] as well.

I would also like to express my huge heartfelt appreciation to the magnanimous
[Donald G. Bailey][dgb], who, through his excellent book [Trax Strategy for
Beginners][traxbook], taught me the basics (and more!) of Trax. I found him
always ready to share his insights on the mechanics of the game, as well as
various approaches to encoding its complexities. His undeserved kindness and
infinite patience in indulging my many questions can never be repaid.

Thanks to [Martin M. S. Pedersen][traxplayer] for contributing ideas, code, and
puzzles. He's been [curating Trax puzzles][puzzlebook] and [coding solutions for
Trax][gnutrax] for over two decades, and has a lot of expertise in this area.

## License

Copyright 2019-2023 Chad Transtrum

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
the files in this project except in compliance with the License. You may obtain
a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

## Trax rules copyright

In the United States, game mechanics are not eligible for copyright protection;
however, the specific wording of game rules does fall under copyright law.

The official rules for Trax are found at http://traxgame.com/about_rules.php.

As Trax is a proprietary game, these rules are the intellectual property of
David Smith and heirs, and are not to be used without permission.

[contributing]: CONTRIBUTING.md
[ctrans]: mailto:chad@transtrum.net
[dgb]: mailto:donald@traxgame.com
[docs]: docs/engine.md
[gnutrax]: https://gnutrax.com
[goldtoken]: https://goldtoken.com/
[puzzlebook]: https://gnutrax.com/book
[repo]: https://gitlab.com/slugbugblue/trax
[sbb]: https://slugbugblue.com/
[trax-cli]: https://gitlab.com/slugbugblue/trax-cli
[traxbook]: http://traxgame.com/shop_book.php
[traxgame]: http://traxgame.com/
[traxhistory]: http://www.traxgame.com/about_history.php
[traxplayer]: mailto:traxplayer@gmail.com
[traxrules]: http://www.traxgame.com/about_rules.php
